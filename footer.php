<footer>
    <div class="container">

        <div class="row">
            <div class="col-12 py-5 my-3 d-flex flex-column justify-content-center align-items-center">
                <h2 class="footer-title">Get in touch</h2>
                <a class="footer-mail" href="mailto:sales@survicate.com">sales@survicate.com</a>
            </div>
        </div>

        <div class="row">

            <div class="col-12 col-lg-2 px-3 d-flex flex-column justify-content-lg-start align-items-lg-start single-menu-wrapper">

                <p class="menu-title">Solution</p>

                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer-solutions',
                    )
                );
                ?>

            </div>
            
            <div class="col-12 col-lg-2 px-3 d-flex flex-column justify-content-lg-start align-items-lg-start single-menu-wrapper">
                
                <p class="menu-title">Product</p>

                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer-product',
                    )
                );
                ?>


            </div>
            
            <div class="col-12 col-lg-2 px-3 d-flex flex-column justify-content-lg-start align-items-lg-start single-menu-wrapper">
                
                <p class="menu-title">Integrations</p>

                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer-integrations',
                    )
                );
                ?>


            </div>
            
            <div class="col-12 col-lg-2 px-3 d-flex flex-column justify-content-lg-start align-items-lg-start single-menu-wrapper">
                
                <p class="menu-title">Resources</p>

                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer-resources',
                    )
                );
                ?>


            </div>
            
            <div class="col-12 col-lg-2 px-3 d-flex flex-column justify-content-lg-start align-items-lg-start single-menu-wrapper">
                
                <p class="menu-title">Company</p>

                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer-company',
                    )
                );
                ?>


            </div>
            
            <div class="col-12 col-lg-2 px-3 d-flex flex-column justify-content-lg-start align-items-lg-start single-menu-wrapper">
                
                <p class="menu-title">Legal</p>

                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer-legal',
                    )
                );
                ?>


            </div>
            

        </div>

        <div class="row">
                
            <div class="col-12 pt-4 d-flex justify-content-center align-items-center">
                <img class="footer-social mx-3" src="<?php echo get_template_directory_uri(); ?>/src/img/facebook.svg" alt="facebook" />
                <img class="footer-social mx-3" src="<?php echo get_template_directory_uri(); ?>/src/img/twitter.svg" alt="twitter" />
                <img class="footer-social mx-3" src="<?php echo get_template_directory_uri(); ?>/src/img/linkedin.svg" alt="linkedin" />
                <img class="footer-social mx-3" src="<?php echo get_template_directory_uri(); ?>/src/img/google-plus.svg" alt="google-plus" />
            </div>
            <div class="col-12 pt-3 d-flex justify-content-center align-items-center">
                <p class="footer-social-sentence">JOIN HUNDREDS OF COMPANIES USING SURVICATE</p>
            </div>

        </div>
        <div class="row">
            <div class="col-12 py-3 d-flex justify-content-center align-items-center">
                <button class="btn">SIGN UP FREE</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12 py-3 d-flex justify-content-center align-items-center">
                <p class="copy-right">&copy; Copyright 2018 survicate.com. All rights reserved</p>
            </div>
        </div>

    </div>
</footer>

<?php wp_footer();?>
</body>
</html>