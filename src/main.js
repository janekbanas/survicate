import $ from 'jquery';
import slick from './slick/slick.min.js';
import navComponent from './scripts/nav';

navComponent();

$('.slider').slick({
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    arrows: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
            infinite: true,
            arrows: false,
            dots: true
        }
    }]
});