<?php
/**
 * Case Study
 */
 
get_header(); ?>
 
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

        <div class="container">
            <div class="row">
                <div class="col-12">
 
                <?php if( get_field('company_logo') ): ?>
                    <img class="py-2 img-fluid" src="<?php the_field('company_logo'); ?>" />
                <?php endif; ?>
                
                
                <?php
                // Start the loop.
             
                while ( have_posts() ) : the_post();
        
        
                    the_content();
              
                    get_template_part( 'content', get_post_format() );
        
                  
                    // Previous/next post navigation.
                    the_post_navigation( array(
                        'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
                            '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
                            '<span class="post-title">%title</span>',
                        'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
                            '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
                            '<span class="post-title">%title</span>',
                    ) );
        
                // End the loop.
                endwhile;
                ?>

                </div>
            </div>
        </div>
 
        </main><!-- .site-main -->
    </div><!-- .content-area -->
 
<?php get_footer(); ?>