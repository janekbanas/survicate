<?php if( have_posts() ): while( have_posts() ): the_post();?>

<div class="container">
    <div class="row d-flex py-4 justify-content-center align-items-center">
        <div class="col-12 order-2 col-lg-6 order-lg-1 pr-lg-5 d-flex flex-column justify-content-between align-items-start">
        
            <?php
            $services_one = get_field('services_one');
            if( $services_one ): ?>
                
                <h2 class="serv-title-bold pb-4"><?php echo $services_one['service_title_bold']; ?>
                    <span class="serv-title-light"><?php echo $services_one['serv-title-light']; ?></span>
                </h2>

                
            <?php endif; ?>

            <?php

            if( have_rows('services_one') ): while ( have_rows('services_one') ) : the_row(); 
            
                if( have_rows('serv_reapeter') ): while ( have_rows('serv_reapeter') ) : the_row();       

                $serv_title = get_sub_field( 'serv_title' );
                $serv_description = get_sub_field( 'serv_description' );

                ?>

                <h3 class="serv-desc-title"><?php echo esc_html( $serv_title ); ?></h3>
                <p class="serv-desc"><?php echo esc_html( $serv_description ); ?></p>

                <?php


                endwhile; endif;

            endwhile; endif;

            ?>

        </div>
        <div class="col-12 order-1 col-lg-6 order-lg-2 d-flex justify-content-start align-items-center">

        <img class="serv-img to-left" src="<?php echo get_template_directory_uri(); ?>/src/img/1create.svg" alt="1create" />


        </div>
    </div>


    <div class="row d-flex py-3 justify-content-center align-items-center">
        <div class="col-12 order-2 col-lg-6 order-lg-2 pl-lg-5 d-flex flex-column justify-content-between align-items-start">
        
            <?php
            $services_two = get_field('services_two');
            if( $services_two ): ?>
                
                <h2 class="serv-title-bold pb-4"><?php echo $services_two['service_title_bold']; ?>
                    <span class="serv-title-light"><?php echo $services_two['serv-title-light']; ?></span>
                </h2>

                
            <?php endif; ?>

            <?php

            if( have_rows('services_two') ): while ( have_rows('services_two') ) : the_row(); 
            
                if( have_rows('serv_reapeter') ): while ( have_rows('serv_reapeter') ) : the_row();       

                $serv_title = get_sub_field( 'serv_title' );
                $serv_description = get_sub_field( 'serv_description' );

                ?>

                <h3 class="serv-desc-title"><?php echo esc_html( $serv_title ); ?></h3>
                <p class="serv-desc"><?php echo esc_html( $serv_description ); ?></p>

                <?php


                endwhile; endif;

            endwhile; endif;

            ?>

        </div>
        <div class="col-12 order-1 col-lg-6 order-lg-1 d-flex justify-content-end align-items-center">

        <img class="serv-img to-right" src="<?php echo get_template_directory_uri(); ?>/src/img/2design.svg" alt="2design" />


        </div>
    </div>


    <div class="row d-flex py-3 justify-content-center align-items-center">
        <div class="col-12 order-2 col-lg-6 order-lg-1 pr-lg-5 d-flex flex-column justify-content-between align-items-start">
        
            <?php
            $services_three = get_field('services_three');
            if( $services_three ): ?>
                
                <h2 class="serv-title-bold pb-4"><?php echo $services_three['service_title_bold']; ?>
                    <span class="serv-title-light"><?php echo $services_three['serv-title-light']; ?></span>
                </h2>

                
            <?php endif; ?>

            <?php

            if( have_rows('services_three') ): while ( have_rows('services_three') ) : the_row(); 
            
                if( have_rows('serv_reapeter') ): while ( have_rows('serv_reapeter') ) : the_row();       

                $serv_title = get_sub_field( 'serv_title' );
                $serv_description = get_sub_field( 'serv_description' );

                ?>

                <h3 class="serv-desc-title"><?php echo esc_html( $serv_title ); ?></h3>
                <p class="serv-desc"><?php echo esc_html( $serv_description ); ?></p>

                <?php


                endwhile; endif;

            endwhile; endif;

            ?>

        </div>
        <div class="col-12 order-1 col-lg-6 order-lg-2 p-0 d-flex justify-content-start align-items-center">

        <img class="serv-img to-left" src="<?php echo get_template_directory_uri(); ?>/src/img/34targetmeasure.svg" alt="3target" />


        </div>
    </div>


    <div class="row d-flex py-3 justify-content-center align-items-center">
        <div class="col-12 order-2 col-lg-6 order-lg-2 pl-lg-5 d-flex flex-column justify-content-between align-items-start">
        
            <?php
            $services_four = get_field('services_four');
            if( $services_four ): ?>
                
                <h2 class="serv-title-bold pb-4"><?php echo $services_four['service_title_bold']; ?>
                    <span class="serv-title-light"><?php echo $services_four['serv-title-light']; ?></span>
                </h2>

                
            <?php endif; ?>

            <?php

            if( have_rows('services_four') ): while ( have_rows('services_four') ) : the_row(); 
            
                if( have_rows('serv_reapeter') ): while ( have_rows('serv_reapeter') ) : the_row();       

                $serv_title = get_sub_field( 'serv_title' );
                $serv_description = get_sub_field( 'serv_description' );

                ?>

                <h3 class="serv-desc-title"><?php echo esc_html( $serv_title ); ?></h3>
                <p class="serv-desc"><?php echo esc_html( $serv_description ); ?></p>

                <?php


                endwhile; endif;

            endwhile; endif;

            ?>

        </div>
        <div class="col-12 order-1 col-lg-6 order-lg-1 p-0 d-flex justify-content-end align-items-center">

        <img class="serv-img to-right" src="<?php echo get_template_directory_uri(); ?>/src/img/34targetmeasure.svg" alt="4measure" />


        </div>
    </div>


</div>

<?php endwhile; else: endif; ?>