<?php if( have_posts() ): while( have_posts() ): the_post();?>

<div class="container">
    <div class="row d-flex flex-row justify-content-between align-items-start">
               
        <?php

        // check if the repeater field has rows of data
        if( have_rows('benefits') ):

            // loop through the rows of data
            while ( have_rows('benefits') ) : the_row();

            $icon = get_sub_field( 'icon' );
            $benefit_title = get_sub_field( 'benefit_title' );
            $benefit_description = get_sub_field( 'benefit_description' );

            ?>
        
            <div class="col-12 col-lg-3 mt-5 mt-lg-1 d-flex flex-column justify-content-center align-items-center">

                <img class="img-fluid benefit-icon" src="<?php echo esc_html( $icon ); ?>" alt="icon" />
                <h3 class="benefit-title py-3"><?php echo esc_html( $benefit_title ); ?></h3>
                <p class="benefit-description"><?php echo esc_html( $benefit_description ); ?></p>
                
            </div>
            
            <?php
            endwhile;

        else :

        endif;

        ?>
    </div>
</div>

<?php endwhile; else: endif; ?>