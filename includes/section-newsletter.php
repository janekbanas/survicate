<?php if( have_posts() ): while( have_posts() ): the_post();?>

<div class="container">
    <div class="row py-4 d-flex align-items-center">

        <div class="col-12 col-lg-4 d-flex justify-content-start align-items-center">

                <h2 class="news-title-bold pb-4"><?php the_field('news-title-bold'); ?>
                    <span class="news-title-light"><?php the_field('news-title-light'); ?></span>
                </h2>
                    
        </div>
        <div class="col-12 col-lg-8">

            <form class="d-flex flex-column flex-lg-row justify-content-end align-items-center align-items-lg-start">
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    
                </div>
                <div class="submit-wrapper d-flex flex-column justify-content-center align-items-center">
                    <button type="submit" class="btn btn-primary">SIGN UP FREE</button>
                    <small id="emailHelp" class="form-text text-muted">Try out Survicate for free</small>
                </div>    
            </form>
        </div>

    </div>
</div>

<?php endwhile; else: endif; ?>