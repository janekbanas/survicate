<?php if( have_posts() ): while( have_posts() ): the_post();?>

<div class="container">
    <div class="row">
        <div class="col-12 py-5">
        
            <h2 class='section-title-bold'><?php the_field('article-sec-title-bold'); ?><span class='section-title-light'><?php the_field('article-sec-title-light'); ?></span>
            </h2>

        </div>
    </div>

    <div class="row mt-3 mb-5 mb-lg-0">
        <?php
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 3
        );

        $loop = new WP_Query( $args );

        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                

                <div class="col-12 col-lg-4 py-3 d-flex flex-column justify-content-between align-items-start">
                    <div class="single-article-wrapper">

                        <?php if( get_field('company_logo') ): ?>
                            <img class="py-2 img-fluid" src="<?php the_field('company_logo'); ?>" />
                        <?php endif; ?>
                        
                        <p class='py-3 sneak-peak'><?php the_field('sneak_peak'); ?></p>
                        <a class='pb-3 read-more' href="<?php echo get_post_permalink(); ?>">Read the story</a>

                    </div>



                </div>

            <?php
            endwhile;
        } ?>

    </div>
       

</div>

<?php endwhile; else: endif; ?>