<?php if( have_posts() ): while( have_posts() ): the_post();?>

<div class="container">
    <div class="row">
        <div class="col-12 px-lg-5 pb-lg-5">
            <div class="slider" data-slick=''>
               
        
                <?php

                if( have_rows('testimonial_slider') ):

                    while ( have_rows('testimonial_slider') ) : the_row();

                    $name = get_sub_field( 'name' );
                    $position = get_sub_field( 'position' );
                    $company = get_sub_field( 'company' );
                    $quotation = get_sub_field( 'quotation' );
                    $image = get_sub_field( 'person_photo' );

                    ?>

                    <div class="single-slide">
                        <div class="row">
                            <img class="img-fluid quote-img" src="<?php echo get_template_directory_uri(); ?>/src/img/quote.svg" alt="quote" />
                            <div class="col-12 col-lg-8 m-0 px-5 pt-5 d-flex flex-column justify-content-center align-items-start">
                                <p class="quote"><?php echo esc_html( $quotation ); ?></p>
                                <div>
                                    <p class="name m-0"><?php echo esc_html( $name ); ?></p>
                                    <p class="position"><?php echo esc_html( $position ); ?> / <span><?php echo esc_html( $company ); ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 m-0 d-flex justify-content-end align-items-center">
                                <img class="img-fluid person-img" src="<?php echo esc_html( $image ); ?>" alt="person-photo" />
                            </div>
                        </div>
                    </div>
                    
                    <?php
                    endwhile;

                else :

                endif;

                ?>

                
            </div>
        </div>
    </div>
</div>

<?php endwhile; else: endif; ?>