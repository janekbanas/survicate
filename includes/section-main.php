<?php if( have_posts() ): while( have_posts() ): the_post();?>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6">
        
            <h1 class='section-title-bold'><?php the_field('title-bold'); ?></h1>
            <h2 class='section-title-light'><?php the_field('title-light'); ?></h2>
            <p class='section-description'><?php the_field('description'); ?></p>

            <button class="btn">SIGN UP FREE</button>

        </div>
        <div class="col-6 d-flex justify-content-center align-items-center">
        
            <img class="top-img" src="<?php echo get_template_directory_uri(); ?>/src/img/top-image.svg" alt="top-image" />


        </div>
    </div>
</div>

<?php endwhile; else: endif; ?>