<?php get_header();?>

<section class="main">

    <?php get_template_part('includes/section', 'main');?>

    <?php get_template_part('includes/section', 'clients');?>

</section>


<section class="testimonials">

    <?php get_template_part('includes/section', 'testimonials');?>

</section>

<section class="benefits">

    <?php get_template_part('includes/section', 'benefits');?>

</section>

<section class="services py-lg-5 my-5">

    <?php get_template_part('includes/section', 'services');?>

</section>

<section class="article">

    <?php get_template_part('includes/section', 'article');?>
    <?php get_template_part('includes/section', 'clients');?>

</section>

<section class="newsletter">

    <?php get_template_part('includes/section', 'newsletter');?>

</section>

<?php get_footer();?>