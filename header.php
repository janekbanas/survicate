<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Survicate wordpress page</title>

    <?php wp_head();?>

</head>

<body>

    <header>

        <nav class="core-nav fixed-start py-4">
            <div class="container">
                <div class="row d-flex align-items-end">

                    <div class="col-2 d-flex justify-content-start">
                        <div class="logo-box">
                            
                            <div id="header-image">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/logo.svg" alt="Logo" width="156" height="42" />
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-10 d-flex justify-content-end align-items-center">
                    
                        <?php 
                        wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                            )
                        );
                        ?>

                        <div class="nav-hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>

                    </div>

                </div>
            </div>
        </nav>

    </header>