<?php


// Load Stylesheets

function load_css()
{
    wp_register_style('slick-theme', get_template_directory_uri() . '/src/slick/slick-theme.css', array(), false, 'all');
    wp_enqueue_style('slick-theme');
    
    wp_register_style('slick', get_template_directory_uri() . '/src/slick/slick.css', array(), false, 'all');
    wp_enqueue_style('slick');

    wp_register_style('bootstrap', get_template_directory_uri() . '/src/bootstrap/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('main', get_template_directory_uri() . '/dist/main.css', array(), false, 'all');
    wp_enqueue_style('main');

}

add_action('wp_enqueue_scripts', 'load_css');

// Load Scripts

function load_js()
{

    wp_register_script('bootstrapJS', get_template_directory_uri() . '/src/bootstrap/bootstrap.min.js', 'jquery', false, true);
    wp_enqueue_script('bootstrapJS');

    wp_register_script('boundle', get_template_directory_uri() . '/dist/boundle.js', '', false, true);
    wp_enqueue_script('boundle');

    wp_enqueue_script('jquery');

}
add_action('wp_enqueue_scripts', 'load_js');

// Theme Options

add_theme_support('menus');

//Menus

register_nav_menus(
    
        array(
            'main-menu' => 'Main Menu Location',
            'menu-footer-solutions' => 'Footer Solutions Location',
            'menu-footer-product' => 'Footer Product Location',
            'menu-footer-integrations' => 'Footer Integrations Location',
            'menu-footer-resources' => 'Footer Resources Location',
            'menu-footer-company' => 'Footer Company Location',
            'menu-footer-legal' => 'Footer Legal Location',
        )
);


// Load ACF

// Define path and URL to the ACF plugin.
define( 'MY_ACF_PATH', get_stylesheet_directory() . '/plugins/acf/' );
define( 'MY_ACF_URL', get_stylesheet_directory_uri() . '/plugins/acf/' );

// Include the ACF plugin.
include_once( MY_ACF_PATH . 'acf.php' );

// Customize the url setting to fix incorrect asset URLs.
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url( $url ) {
    return MY_ACF_URL;
}

// (Optional) Hide the ACF admin menu item.
add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
function my_acf_settings_show_admin( $show_admin ) {
    return false;
}

include_once( get_stylesheet_directory() . '/plugins/acf/acf-field-groups.php' );

// Load SVG

include_once( get_stylesheet_directory() . '/plugins/safe-svg/safe-svg.php' );